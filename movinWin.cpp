#include"bguic.h"
void main()
{
   int gdriver=DETECT, gmode, errorcode,b,x,y;
   initgraph(&gdriver, &gmode, "C:\\tc\\bgi");
   errorcode = graphresult();
   if (errorcode!=grOk)
   {
	  printf("Press any key to halt:");
	  getch();
	  exit(1);
   }
	int x1=200,y1=140,x2=440,y2=320;
	int mousex,mousey,changedy=0,changedx,exitcode=1;
	Mouse mouse;
	mouse.initialise_mouse();
	//Mouse.pointer(HIDE);
	Window movingwin(x1,y1,x2,y2,7,"Move this");
	Window menuwin(10,10,629,469,7,"Power of c graphics");
	Button exitbutt(x1+85,y1+100,x2-80,y2-50,7,1,"Ok");
	TextField *tfield=new TextField(x1+75,y1+70,11);
	menuwin.draw();
	movingwin.draw();
	exitbutt.draw();
	tfield->displayTextField();
	tfield->setText("Click Here");
	mouse.pointer(SHOW);
	setlinestyle(0,1,1);
	setcolor(0);
	outtextxy(movingwin.X1+60,movingwin.Y1+40,"See this effect");
	outtextxy(movingwin.X1+55,movingwin.Y1+55,"How great is c++");
	while(exitcode)
		{
			mouse.get_pointer_position(&b,&x,&y);
			if(exitbutt.click())
			{cleardevice();cout<<"You entered"<<tfield->getText();getch();exit(0);}
			if(tfield->isChoosed())
			{tfield->handleTextField();}
			if(b==1&&x>movingwin.X1+1&&x<movingwin.X2-1&&y>movingwin.Y1+1&&y<movingwin.Y1+movingwin.barheight)
			 {
				mouse.get_pointer_position(&b,&x,&y);
				mousex=x;
				mousey=y;
				while(b==1&&x>movingwin.X1+1&&x<movingwin.X2-1&&y>movingwin.Y1+1&&y<movingwin.Y1+movingwin.barheight)
					{
						mousex=x;
						mousey=y;
						mouse.get_pointer_position(&b,&x,&y);
						if(y<mousey)//if mouse moved up
							{
									changedy=mousey-y;
									movingwin.Y1-=changedy;
									movingwin.Y2-=changedy;
									exitbutt.Y1=movingwin.Y1+100;
									exitbutt.Y2=exitbutt.Y1+30;
									tfield->yPos=movingwin.Y1+70;
									mouse.pointer(HIDE);
									movingwin.draw();
									exitbutt.draw();
									tfield->displayTextField();
									tfield->setText(tfield->str);
									setfillstyle(1,7);
									bar(movingwin.X1,movingwin.Y2+1,movingwin.X2,movingwin.Y2+changedy);
									mouse.pointer(SHOW);
							}
						   if(y>mousey)//if mouse moved down
							{

									changedy=y-mousey;
									movingwin.Y1+=changedy;
									movingwin.Y2+=changedy;
									exitbutt.Y1=movingwin.Y1+100;
									exitbutt.Y2=exitbutt.Y1+30;
									tfield->yPos=movingwin.Y1+70;
									mouse.pointer(HIDE);
									movingwin.draw();
									exitbutt.draw();
									tfield->displayTextField();
									tfield->setText(tfield->str);
									setfillstyle(1,7);
									bar(movingwin.X1,movingwin.Y1-changedy,movingwin.X2,movingwin.Y1-1);
									mouse.pointer(SHOW);
							}
							if(x<mousex)//if mouse moved left
								{
									changedx=mousex-x;
									movingwin.X1-=changedx;
									movingwin.X2-=changedx;
									exitbutt.X1=movingwin.X1+85;
									exitbutt.X2=movingwin.X2-80;
									tfield->xPos=movingwin.X1+75;
									mouse.pointer(HIDE);
									movingwin.draw();
									exitbutt.draw();
									tfield->displayTextField();
									tfield->setText(tfield->str);
									setfillstyle(1,7);
									bar(movingwin.X2+1,movingwin.Y1,movingwin.X2+changedx,movingwin.Y2);
									mouse.pointer(SHOW);

								 }
							if(x>mousex)//if mouse moved right
								{
									changedx=x-mousex;
									movingwin.X1+=changedx;
									movingwin.X2+=changedx;
									exitbutt.X1=movingwin.X1+85;
									exitbutt.X2=movingwin.X2-80;
									tfield->xPos=movingwin.X1+75;
									mouse.pointer(HIDE);
									movingwin.draw();
									exitbutt.draw();
									tfield->displayTextField();
									tfield->setText(tfield->str);
									setfillstyle(1,7);
									bar(movingwin.X1-changedx,movingwin.Y1,movingwin.X1-1,movingwin.Y2);
									mouse.pointer(SHOW);
							 }
							 outtextxy(movingwin.X1+60,movingwin.Y1+40,"See this effect");
							 outtextxy(movingwin.X1+55,movingwin.Y1+55,"How great is c++");
				  }
	}
 }
	 closegraph();
	 restorecrtmode();
}






