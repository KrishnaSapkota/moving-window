#include<iostream.h>
#include<string.h>
#include<stdlib.h>
#include<conio.h>
#include<stdio.h>
#include<dos.h>
#include<alloc.h>
#include<graphics.h>
#define ClicKed 1
#define Not_ClicKed 0
#define SHOW 1
#define HIDE 2
enum boolean{false,true};
const int PASSWORDFIELD=1;
const int DEFAULTFIELD=2;
class WINDOWS
{
		 private:
					 int Window_Color,bar_color;
					 char Window_Name[60];
					 struct date d;
					 unsigned long size_of_window;
					 void far  *buff;
					 void WRITE_DATE(int,int);
					 boolean write_date;

		 public:
					 int barheight;
					 int X1,Y1,X2,Y2;
					 WINDOWS(int,int,int,int,int,char [],int barcol=BLUE,boolean date=true);
					 WINDOWS(WINDOWS &,int,char []);
					 void draw();
					 void clear();

};
class MOUSE
{
	private:
				union REGS input,output;
	public:
				MOUSE(){};
				int initialse_mouse();
				pointer(int ON);
				restrict_pointer(int X1,int Y1,int X2,int Y2);
				change_pointer(int X1,int Y1);
				int get_pointer_position(int *Button,int *X,int *Y);
};
class BUTTONS:public MOUSE
{
		private:
				   int button,x,y;
				   int Text_Color,Button_Color;
				   char Button_Name[30];
		public:
				   int X1,Y1,X2,Y2;
				   BUTTONS(){};
				   BUTTONS(int x1,int y1,int x2,int y2,int col,int t_col,char But_Name[]);
				   BUTTONS(BUTTONS &,int,int,char []);
				   void initialiseButton(int x1,int y1,int x2,int y2,int col,int t_col,char But_Name[]);
				   void Unpushed_Button(int,int,int,int,int);
				   void Pushed_Button(int,int,int,int,int);
				   void draw();
				   int click();

};
class TEXTFIELD
{
	private:
			int fieldcolor,textcolor,cursorcolor;
			int length,b,x,y;
			int HEIGHT,WNO;
			int c,w,type;
			char *String,ch,*password;
			MOUSE Mousehandler;

	public:
			char *str;
			int xPos,yPos;
			enum key{notpressed,pressed};
			key escapekey;
			boolean textpresent;
			TEXTFIELD(){};
			TEXTFIELD(int x,int y,int l,int wordno,const int ty=DEFAULTFIELD)
				{

					WNO=wordno;
					type=ty;
					if(type==PASSWORDFIELD)
					password=new char[WNO];
					str=new char[WNO];
					HEIGHT=20;
					fieldcolor=15;
					textcolor=0;
					cursorcolor=0;
					xPos=x;
					yPos=y;
					length=l;
					escapekey=notpressed;
					textpresent=false;
					c=0;
					w=1;
					String=NULL;
				}
			 void initialiseTextField(int ,int, int, int,int ty=DEFAULTFIELD);
			 void displayTextField();
			 char *handleTextField();
			 boolean isChoosed();
			 void setText(char [],int check=0);
};
class MESSAGEBOX:public BUTTONS
{
		private:    int fillcolor,barcolor;
					char message_title[15],message[40];
					int X1,Y1,X2,Y2,value;

		public:
					MESSAGEBOX(int X1,int Y1,int X2,int Y2,int color,int barcol,char title[],char messag[],int val=NULL);
					void showmessagebox();
};


class IMAGE
{
	private:
			int IMAGES[81][176];
			int x1pix,y1pix;
			int  maxrow,maxcol;

	public:
			IMAGE(int x1p,int y1p)
				{
					x1pix=x1p;
					y1pix=y1p;
					maxrow=176;
					maxcol=81;
				}
			 void GetImage();
			 void PutImage();
};

void IMAGE::GetImage()
{
   for(int i=0;i<maxcol;i++)
   for (int j=0;j<maxrow;j++)
   IMAGES[i][j]=getpixel(x1pix+j,y1pix+i);
   IMAGES[++i][++j]='/0';
   return;

}
void IMAGE::PutImage()
{

   for(int i=0;i<maxcol;i++)
	 {
		for (int j=0;j<maxrow;j++)
			{
				putpixel(x1pix+j,y1pix+i,IMAGES[i][j]);
			}
	 }
   return;
}


WINDOWS::WINDOWS(int x1,int y1,int x2,int y2,int color,char Win_name[],int barcolor,boolean date)
{
	strcpy(Window_Name,Win_name);
	Window_Color=color;
	bar_color=barcolor;
	barheight=20;
	write_date=date;
	X1=x1;
	Y1=y1;
	X2=x2;
	Y2=y2;
}
WINDOWS::WINDOWS(WINDOWS &window,int color,char Win_name[])
{
	strcpy(Window_Name,Win_name);
	Window_Color=color;
	bar_color=window.bar_color;
	barheight=20;
	write_date=window.write_date;
	X1=window.X1;
	Y1=window.Y1;
	X2=window.X2;
	Y2=window.Y2;
}

BUTTONS::BUTTONS(int x1,int y1,int x2,int y2,int col,int t_col,char But_Name[])
{
	strcpy(Button_Name,But_Name);
	Button_Color=col;
	Text_Color=t_col;
	X1=x1;
	Y1=y1;
	X2=x2;
	Y2=y2;
}
BUTTONS::BUTTONS(BUTTONS &button,int col,int tcol,char nam[])
{
	X1=button.X1;
	Y1=button.Y1;
	X2=button.X2;
	Y2=button.Y2;
	Text_Color=tcol;
	Button_Color=col;
	strcpy(Button_Name,nam);
}

void BUTTONS::initialiseButton(int x1,int y1,int x2,int y2,int col,int t_col,char But_Name[])
{
	strcpy(Button_Name,But_Name);
	Button_Color=col;
	Text_Color=t_col;
	X1=x1;
	Y1=y1;
	X2=x2;
	Y2=y2;

}


void WINDOWS::clear()
{
	size_of_window=imagesize(X1,Y1,X2,Y2);
	buff=farmalloc(size_of_window);
   //	getimage(X1,Y1,X2,Y2,buff);
	putimage(X1,Y1,buff,XOR_PUT);
	farfree(buff);
}
int BUTTONS::click()
{
	int t1,t2,i,k=0;
	setlinestyle(0,1,1);
	for(i=0;i<strlen(Button_Name);i++)
	k=k+4;
	t1=(X2-X1)/2+X1-k;
	t2=(Y2-Y1)/2+Y1;
	get_pointer_position(&button,&x,&y);
	if( (x>X1 && x<X2) && (y>Y1 && y<Y2) && (button==1))
	{
		  pointer(HIDE);
		  Pushed_Button(X1,Y1,X2,Y2,Button_Color);
		  setcolor(Text_Color);
		  settextstyle(0,0,0);
		  outtextxy(t1+2,t2+2,Button_Name);
		  pointer(SHOW);
		  while((button==1))
		  get_pointer_position(&button,&x,&y);
		  pointer(HIDE);
		  Unpushed_Button(X1,Y1,X2,Y2,Button_Color);
		  settextstyle(0,0,0);
		  setcolor(Text_Color);
		  outtextxy(t1,t2,Button_Name);
		  for(i=0;i<500;i=i+50)
		  {
		delay(15);
		sound(i+200);
		  }
		 nosound();
		 pointer(SHOW);
		 return ClicKed;
	}
	else{
			return Not_ClicKed;
		}
}
void BUTTONS::Unpushed_Button(int X1,int Y1,int X2,int Y2,int col)
{
	Button_Color=col;
	setlinestyle(0,1,1);
	setfillstyle(1,Button_Color);
	bar(X1,Y1,X2,Y2);
	setcolor(WHITE);
	line(X1,Y1,X2,Y1);
	line(X1,Y1,X1,Y2);
	setcolor(BLACK);
	line(X2,Y1,X2,Y2);
	line(X1,Y2,X2,Y2);

}
void BUTTONS::Pushed_Button(int X1,int Y1,int X2,int Y2,int col)
{
	Button_Color=col;
	setlinestyle(0,1,1);
	setfillstyle(1,Button_Color);
	bar(X1,Y1,X2,Y2);
	setcolor(BLACK);
	line(X1,Y1,X2,Y1);
	line(X1,Y1,X1,Y2);
	setcolor(WHITE);
	line(X2,Y1,X2,Y2);
	line(X1,Y2,X2,Y2);
}
void BUTTONS::draw()
{
	int t1,t2,k=0,i;
	setlinestyle(0,1,1);
	for(i=0;i<strlen(Button_Name);i++)
	k=k+4;
	t1=(X2-X1)/2+X1-k;
	t2=(Y2-Y1)/2+Y1;
	Unpushed_Button(X1,Y1,X2,Y2,Button_Color);
	settextstyle(0,0,0);
	setcolor(Text_Color);
	outtextxy(t1,t2,Button_Name);
}
void WINDOWS::draw()
{
		 setlinestyle(0,1,1);
		 setfillstyle(SOLID_FILL,Window_Color);
		 bar(X1,Y1,X2,Y2);
		 setcolor(WHITE);
		 if(Window_Color!=bar_color){
		 line(X1,Y1,X2,Y1);
		 line(X1,Y1,X1,Y2);
		 setcolor(BLACK);
		 line(X1,Y2,X2,Y2);
		 line(X2,Y1,X2,Y2);}
		 if(Window_Color==bar_color){
		 setcolor(BLACK);
		 line(X1,Y1,X2,Y1);
		 line(X1,Y1,X1,Y2);
		 setcolor(WHITE);
		 line(X2,Y1,X2,Y2);
		 line(X2,Y2,X1,Y2);}
		 if(Window_Color==BLUE)
		 {setfillstyle(SOLID_FILL,RED);}
		 else{setfillstyle(SOLID_FILL,bar_color);}
		 bar(X1+1,Y1+1,X2-1,Y1+barheight);
		 if((!strcmp(Window_Name,"")));
		 {setcolor(YELLOW);
		 outtextxy(X1+3,Y1+10,Window_Name);}
		 if(write_date)
		 WRITE_DATE(X2-80,Y1-200);
		 setcolor(0);
}
void WINDOWS::WRITE_DATE(int X2, int Y1)
{
	int a,b,c;
	char *month;
	getdate(&d);
	char d1[10],d2[10],d3[10];
	setcolor(YELLOW);
	a=d.da_year;
	b=d.da_day;
	c=d.da_mon;
	switch(c)
		{
			case 1:	outtextxy(X2-60,Y1+210,"January");break;
			case 2:	outtextxy(X2-64,Y1+210,"Feburary");break;
			case 3:	outtextxy(X2-64,Y1+210,"March");break;
			case 4:	outtextxy(X2-64,Y1+210,"April");break;
			case 5:	outtextxy(X2-30,Y1+210,"May");break;
			case 6: outtextxy(X2-30,Y1+210,"June");break;
			case 7: outtextxy(X2-30,Y1+210,"July");break;
			case 8: outtextxy(X2-45,Y1+210,"August");break;
			case 9: outtextxy(X2-64,Y1+210,"September");break;
			case 10: outtextxy(X2-56,Y1+210,"October");break;
			case 11: outtextxy(X2-64,Y1+210,"November");break;
			case 12: outtextxy(X2-64,Y1+210,"December");break;
		  }
	itoa(a,d1, 10);
	itoa(b,d2, 10);
	itoa(c,d3, 10);
	outtextxy(X2+5,Y1+210,d2);
	outtextxy(X2+33,Y1+210,d1);
	setcolor(BLACK);
}
void TEXTFIELD::initialiseTextField(int x,int y,int l,int wordno,int ty)
{
	WNO=wordno;
	type=ty;
	str=new char[WNO];
	if(type==PASSWORDFIELD)
	password=new char[WNO];
	HEIGHT=20;
	fieldcolor=15;
	textcolor=0;
	cursorcolor=0;
	textpresent=false;
	xPos=x;
	yPos=y;
	length=l;
	String=NULL;
}

void TEXTFIELD::displayTextField()
{
	WINDOWS field(xPos,yPos,xPos+length,yPos+HEIGHT,fieldcolor,"",fieldcolor,false);
	field.draw();
}
boolean TEXTFIELD::isChoosed()
{

	Mousehandler.get_pointer_position(&b,&x,&y);
	if(b&1==1&&x>=xPos&&y>=yPos&&x<=xPos+length&&y<=yPos+HEIGHT)
	return true;
	else
	return false;
}
void TEXTFIELD::setText(char text[],int check)
{
	if(type==DEFAULTFIELD&&textpresent)
		{
			setcolor(textcolor);
			outtextxy(xPos+4,yPos+6,str);
			return;
		}
	else if(type==DEFAULTFIELD&&check)
		{
			setcolor(textcolor);
			outtextxy(xPos+4,yPos+6,text);
			textpresent=true;
			strcpy(str,text);
			return;
		}

}
char *TEXTFIELD::handleTextField()
{

	WINDOWS field(xPos,yPos,xPos+length,yPos+HEIGHT,fieldcolor,"",fieldcolor,false);
	field.draw();
	Mousehandler.pointer(HIDE);
	if(textpresent==true&&type==DEFAULTFIELD)
	{
		w=strlen(str)+1;
		c=w-1;
		setcolor(textcolor);
		outtextxy(xPos+4,yPos+6,str);
	}
	else
	{
		 again:
		 c=0;
		 w=1;
		 field.draw();
	}
			 settextstyle(0,0,1);
				  do
					 {
						while(!kbhit())
						{
							setcolor(cursorcolor);
							line(xPos+w*8,yPos+4,xPos+w*8,yPos+HEIGHT-5);
							delay(100);
							setcolor(fieldcolor);
							line(xPos+w*8,yPos+4,xPos+w*8,yPos+HEIGHT-5);
							delay(120);
						}
						ch=getch();
						setcolor(fieldcolor);
						line(xPos+w*8,yPos+4,xPos+w*8,yPos+HEIGHT-5);
						if(ch==8&&c>0)
						{
						  setfillstyle(1,fieldcolor);
						  bar((xPos+w*8)-12,yPos+3,xPos+w*8+8,yPos+HEIGHT-5);
						  c--;w--;
						  str[c]='\0';
						}
						if(c<0)
						c=0;
						if(w<0)
						w=1;
						if(ch==27)
						{escapekey=pressed;textpresent=false;field.draw();Mousehandler.pointer(SHOW);return NULL;}
						if(ch!='\r'&&ch!=8)
						   {
							  str[c++]=ch;
							  str[c]='\0';
							  if(type==PASSWORDFIELD)
								{
								  for(int i=0;i<c;i++)
								  password[i]='*';
								  password[c]='\0';
								}
							  w++;
						   }
						if(ch!=8&&type==DEFAULTFIELD)
						{
							setcolor(textcolor);
							outtextxy(xPos+4,yPos+6,str);
						}
						if(ch!=8&&type==PASSWORDFIELD)
						{
							setcolor(textcolor);
							outtextxy(xPos+4,yPos+6,password);
						}
						if(c>WNO)
						{c=0;w=1;goto again;}
						if(c==0)
						{goto again;}

					 }while(ch!='\r');
  Mousehandler.pointer(SHOW);
  if(type==DEFAULTFIELD)
  textpresent=true;
  return str;
}

MESSAGEBOX::MESSAGEBOX(int X1,int Y1,int X2,int Y2,int color,int barcol,char title[],char messag[],int val)
{


	this->X1=X1;
	this->Y1=Y1;
	this->X2=X2;
	this->Y2=Y2;
	fillcolor=color;
	barcolor=barcol;
	strcpy(message_title,title);
	strcpy(message,messag);
	value=val;
}
void MESSAGEBOX::showmessagebox()
{
	   int t1,t2,k=0,i;
	   char valu[10];
	   int width=X2-X1;
	   int buttxpos=((X2-X1)/2+X1)-40;
	   setlinestyle(0,1,1);
	   int messglength=strlen(message);
	   if(width>=messglength*5)
	   {
			for(i=0;i<messglength;i++)
				k=k+4;
				t1=(X2-X1)/2+X1-k;
				t2=(Y2-Y1)/2+Y1;
	   }
	   WINDOWS mess(X1,Y1,X2,Y2,fillcolor,message_title,barcolor,false);
	   BUTTONS closebutt(X2-30,Y1+3,X2-13,Y1+18,7,0,"X");
	   BUTTONS okbutt(buttxpos,Y2-50,buttxpos+70,Y2-20,7,0,"OK");
	   mess.draw();
	   closebutt.draw();
	   okbutt.draw();
	   outtextxy(t1,t2,message);
	   if(value!=NULL)
	   {
			outtextxy(t1+27,t2+15,itoa(value,valu,10));
	   }
	   while(1)
	   {
		if(closebutt.click()){return;}
		if(okbutt.click()){return;}
		}
}

int MOUSE::initialse_mouse()
{
	input.x.ax=0;
	int86(0x33,&input,&output);
	return(input.x.ax);
}
int MOUSE::pointer(int ON)
{
	input.x.ax=ON;
	int86(0x33,&input,&output);
	return 0;
}
int MOUSE::restrict_pointer(int X1,int Y1,int X2,int Y2)
{
	input.x.ax=7;
	input.x.cx=X1;
	input.x.dx=X2;
	int86(0x33,&input,&output);
	input.x.ax=8;
	input.x.cx=Y1;
	input.x.dx=Y2;
	int86(0x33,&input,&output);
	return(0);
}
int MOUSE::change_pointer(int X1,int Y1)
{
	input.x.ax=4;
	int86(0x33,&input,&output);
	input.x.cx=X1;
	input.x.dx=Y1;
	return(0);
}
int MOUSE::get_pointer_position(int *Button,int *X,int *Y)
{
	 input.x.ax=3;
	 int86(0x33,&input,&output);
	 *Button=output.x.bx;
	 *X=output.x.cx;
	 *Y=output.x.dx;
	 return 0;
}
